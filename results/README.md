## Results dir
Here you will have a similar folder structure like in the datasets dir, with 
all the results, ordered as in datasets dir.

The *metrics* directory stores a summary of all algorithms applied to the
studied dataset.