import argparse
from datetime import datetime
import os
import sys
import random
import shutil

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from algorithms.util.utils import write_parameters

parser = argparse.ArgumentParser(description='CompreX')
parser.add_argument('--input', '-i',
                    help='input file', required=True)
parser.add_argument('--output', '-o',
                    help='output file', required=True)
parser.add_argument('--maxiterations', '-m',
                    help='the maximum number of iterations that CompreX will do; if left unset,'
                         'it will stop when it converges', default='1000000')


def run(inputt, output, maxiterations):
    start = datetime.now()

    print(f'Running CompreX with maxiterations = {maxiterations}')

    prefix = '/tmp/comprex%s' % (random.randint(1, 10000000))
    os.system(f'sh algorithms/comprex/comprex.sh {inputt} {output} {maxiterations} {prefix}')

    with open(f'{prefix}/cost.txt') as f:
        cost = float(f.read())

    shutil.rmtree(prefix)

    end = datetime.now()
    write_parameters(output, end-start, {'Algorithm': 'CompreX', 'Mode': 'batch', 'totalSize': cost,
                                         'minSup': maxiterations})


if __name__ == '__main__':
    args = parser.parse_args()

    run(args.input, args.output, args.maxiterations)
