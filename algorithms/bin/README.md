## Binaries information
Some C++ code has been modified and the binaries of Krimp and Slim have been 
recompiled in order to work for iAnomaly. You can ask for these modified
files if you want to make further changes.

- Source code for the binary used by `krimp.py` (`krimp`) can be downloaded [here](https://people.mmci.uni-saarland.de/~jilles/prj/krimp/) 
- Source code for the binary used by `slim_upc.py` (`fic`) can be downloaded [here](http://eda.mmci.uni-saarland.de/prj/upc/) 
- Source code for the binary used by `slim.py` (`slim`) (NOT LONGER USED BY SNAKEFILE) can be downloaded [here](http://eda.mmci.uni-saarland.de/prj/slim/) 