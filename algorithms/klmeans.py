import argparse
import os
import sys
from datetime import datetime

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import algorithms.simple.model as model
import algorithms.simple.klmeans as klmeans
import algorithms.simple.klmeansfast as klmeansfast
import algorithms.simple.klstream as klstream
import algorithms.simple.klmeans_simple as klmeans_simple
from algorithms.util.utils import write_parameters

parser = argparse.ArgumentParser(description='KL Means')
parser.add_argument('--input', '-i',
					help='input file',
					required=True)
parser.add_argument('--output', '-o',
					help='output file',
					required=True)
parser.add_argument('--cluster', '-c',
					help='cluster model file',
					default=None)
parser.add_argument('--mode', '-m',
					help='batch, fastbatch, stream or simple (just for the desired number of clusters k)',
					default='batch',
					choices=['batch','fastbatch','stream','simple'])
parser.add_argument('--classes', '-k', help='number of classes',default=1)
parser.add_argument('--epsilon', '-e', help='improvement threshold',default=0.01)


if __name__ == '__main__':
	start = datetime.now()
	args = parser.parse_args()
	dbsize = modelcost = totalsize = initialsize = entropy = '-'
	if args.mode == 'batch':
		initialsize, dbsize, modelcost, totalsize, entropy = klmeans.run(args.input, args.output, int(args.classes),
															epsilon=float(args.epsilon),
															modelfile=args.cluster)
	elif args.mode == 'simple':
		initialsize, dbsize, modelcost, totalsize, entropy = klmeans_simple.run(args.input, args.output, int(args.classes),
															epsilon=float(args.epsilon),
															modelfile=args.cluster)
	elif args.mode == 'fastbatch':
		initialsize, dbsize, modelcost, totalsize = klmeansfast.run(args.input, args.output, int(args.classes),
													epsilon=float(args.epsilon),
													modelfile=args.cluster)
	elif args.mode == 'stream':
		totalsize, entropy = klstream.run(args.input, args.output, modelfile=args.cluster)

	end = datetime.now()
	write_parameters(args.output, end-start, {'Algorithm': 'klmeans',
											  'Mode': args.mode,
											  'kmax': args.classes,
											  'dbCompressedSize': dbsize,
											  'modelSize': modelcost,
											  'totalSize': totalsize,
											  'dbInitialSize': initialsize,
											  'entropy': entropy})