
import argparse
from datetime import datetime
import os
import random
import sys
import csv
import numpy as np
from itertools import islice
import shutil


sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import algorithms.util.context as context
from algorithms.util.utils import write_parameters

parser = argparse.ArgumentParser(description='Clustering approach using CompreX')
parser.add_argument('--input', '-i',
                    help='input score file', required=True)
parser.add_argument('--output', '-o',
                    help='output file', required=True)
parser.add_argument('--classes', '-k', help='number of classes', default=1, type=int)
parser.add_argument('--epsilon', '-e', help='improvement threshold', default=0.01, type=float)
parser.add_argument('--maxiterations', '-m',
                    help='the maximum number of iterations that each CompreX will do; if left unset,'
                         'it will stop when it converges', default='1000000')
parser.add_argument('--mode', '-mo',
                    help='batch or simple (just for the desired number of clusters k)',
                    default='batch',
                    choices=['batch', 'simple'])


def get_context(ctxtfile):
    with open(ctxtfile) as infile:
        reader = csv.reader(infile)
        ctxt = context.Context(reader)
    return ctxt


def random_categories(data, k):
    d = {c: [] for c in range(0, k)}
    for uuid in data:
        d[random.randint(0, k - 1)].append(uuid)
    return d


def write_row(outfile, ctxt, uuid):
    outfile.write('%s,%s\n' % (uuid, ','.join([str(ctxt.data[uuid][att]) for att in ctxt.header])))


def write_singletons(l, cluster_file):
    a = np.zeros(l, dtype=int)
    for i in range(len(a)):
        a[i] = 1
        s = '-,' + str(a[0])
        for j in range(len(a) - 1):
            s += ',' + str(a[j + 1])
        cluster_file.write(s+'\n')
        a[i] = 0


def write_splits(d, ctxt, prefix):
    for i in d.keys():
        with open('%s/ctxt%s/cluster.csv' % (prefix, i), 'w') as outfile:
            outfile.write("Object_ID,%s\n" % ','.join(ctxt.header))
            for uuid in d[i]:
                write_row(outfile, ctxt, uuid)
            # Add singleton (0,0,...,1,...,0,0) for each attribute
            # write_singletons(len(ctxt.header), outfile)
            # Add a line of ones in each cluaster
            # outfile.write('-' + ',1'*len(ctxt.header))


def create_models(d, prefix, maxit):
    for i in d.keys():
        filestem = prefix + '/ctxt' + str(i)
        inputdata = '%s/cluster.csv' % filestem
        os.system(f'sh algorithms/comprex/generate_models.sh {inputdata} {filestem} {maxit}')


def score_final_splits(d, prefix, maxit):
    for i in d.keys():
        filestem = prefix + '/ctxt' + str(i)
        os.system(f'sh algorithms/comprex/score_final_splits.sh {filestem}')


def get_total_cost(d, prefix):
    total = 0
    for i in d.keys():
        with open(f'{prefix}/ctxt{i}/cost.txt') as f:
            total += float(f.read())
    return total


def score_by_model(d, prefix):
    # Score the whole dataset with each model
    for i in d.keys():
        filestem = prefix + '/ctxt' + str(i)
        os.system(f'sh algorithms/comprex/score_by_model.sh {prefix}/entire_input.mat {filestem}')
        # os.system(f'sh algorithms/comprex/score_by_model.sh {filestem}/input.mat {filestem}')


def reassign_categories(uuids, prefix, k):
    d = {c: [] for c in range(0, k)}
    dictionary = dict()
    for i in range(k):
        with open(f'{prefix}/ctxt{i}/scores.csv') as f:
            reader = csv.reader(f, delimiter=',')
            # Create dictionary, clusters as keys, list of transaction scores as values
            scores = []
            next(reader)
            for row in reader:
                scores.append(float(row[1]))
            dictionary.update({i: scores})

    for i in range(len(uuids)):  # Iterate through every transaction
        choose = []
        for j in range(k):  # Iterate through every cluster
            choose.append(dictionary[j][i])
        d[np.argmin(choose)].append(uuids[i])

    return d


if __name__ == '__main__':
    args = parser.parse_args()
    start = datetime.now()

    print('Running CompreX* for K_MAX = %d' % args.classes)
    prefix = '/tmp/comprexstar%s' % random.randint(1, 10000000)
    os.mkdir(prefix)

    os.system(f'python3 algorithms/comprex/convertmat.py -i {args.input} -o {prefix}/entire_input.mat')

    [os.mkdir(f'{prefix}/ctxt{i}') for i in range(args.classes)]

    ctxt = get_context(args.input)
    n = len(ctxt.data)
    m = len(ctxt.header)
    data = {uuid: {att for att in ctxt.data[uuid].keys()
                   if ctxt.data[uuid][att] == 1}
            for uuid in ctxt.data.keys()}

    # Full version of the algorithm (selecting the best-fitting number of clusters after scanning up to k)
    if args.mode == 'batch':
        totalmincost = None
        for k in range(1, args.classes + 1):
            print('k = %d' % k)

            # STEP 1: Allocate randomly each transaction to a cluster
            d = random_categories(ctxt.data.keys(), k)

            lastcost = None
            for i in range(10):
                # STEP 2: Create a model for each cluster based on its transactions
                write_splits(d, ctxt, prefix)  # Generates cluster.csv files
                create_models(d, prefix, args.maxiterations)

                # STEP 3: Compute the total compressed size of the dataset
                totalcost = get_total_cost(d, prefix)
                print(f'Iteration {i+1} with cost = {totalcost}')
                # If it did not improve, the exit the loop
                if k == 1 or (lastcost != None and totalcost > lastcost * (1 - args.epsilon)):
                    # TODO: recover last results (clusters + models). The current cost could be worse than the previous one
                    break
                # Otherwise, save this result
                lastcost = totalcost

                # STEP 4: Score the entire dataset with each cluster's model
                score_by_model(d, prefix)

                # STEP 5: Reallocate each transaction to the best model (over k) for it
                d = reassign_categories(list(ctxt.data.keys()), prefix, k)

            # Save best MDL-fitting model
            if totalmincost == None or totalcost < totalmincost:
                min_k = k
                totalmincost = totalcost
                score_final_splits(d, prefix, args.maxiterations)  # Extract scores

        print(f'FINAL: Min k = {min_k} with cost {totalmincost}')

    # Simple version (only using k clusters)
    else:
        k = args.classes
        print('Fixed K = %d' % k)
        d = random_categories(ctxt.data.keys(), k)
        lastcost = None
        for i in range(10):
            write_splits(d, ctxt, prefix)  # Generates cluster.csv files
            create_models(d, prefix, args.maxiterations)
            totalcost = get_total_cost(d, prefix)
            print(f'Iteration {i+1} with cost = {totalcost}')
            if k == 1 or (lastcost != None and totalcost > lastcost * (1 - args.epsilon)):
                # TODO: recover last results (clusters + models). The current cost could be worse than the previous one
                break
            lastcost = totalcost
            score_by_model(d, prefix)
            d = reassign_categories(list(ctxt.data.keys()), prefix, k)

        score_final_splits(d, prefix, args.maxiterations)  # Extract scores
        min_k = k
        totalmincost = totalcost

    # Merge different cluster final score files and save it to output file
    with open(f'{args.output}', 'w') as result:
        result.write("Object ID, CompreX* Score\n")
        for i in range(min_k):
            with open(f'{prefix}/ctxt{i}/mergedscores.csv', 'r') as merged_score_file:
                for line in islice(merged_score_file, 1, None):
                    result.write(line)

    shutil.rmtree(prefix)

    end = datetime.now()
    write_parameters(args.output, end - start, {'Algorithm': 'CompreXstar',
                                                'Mode': args.mode,
                                                'kmax': args.classes,
                                                'minSup': args.maxiterations,
                                                'totalSize': totalmincost})
