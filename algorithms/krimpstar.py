import argparse
import csv
import numpy as np
import os
import random
import shutil
import sys
from datetime import datetime
import yaml

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import algorithms.krimp as krimp
import algorithms.util.context as context
import algorithms.util.check as check
from algorithms.util.utils import write_parameters

parser = argparse.ArgumentParser(description='Clustering approach using Krimp/OC3')
parser.add_argument('--input', '-i',
                    help='input score file', required=True)
parser.add_argument('--output', '-o',
                    help='output file', required=True)
parser.add_argument('--classes', '-k', help='number of classes', default=1, type=int)
parser.add_argument('--epsilon', '-e', help='improvement threshold', default=0.01, type=float)
parser.add_argument('--mode', '-m',
                    help='batch or simple (just for the desired number of clusters k)',
                    default='batch',
                    choices=['batch', 'simple'])


# a naive estimate of size needed to represent codetable: boolean matrix
# plus frequency count
def codeSize1(cte, numattrs, numtrans):
    size = 0
    size = size + np.log2(numtrans + 1)
    if len(cte['attributes']) > 1:
        size = size + numattrs
    return size


# a more sophisticated encoding for sparse code tables
def codeSize2(cte, numattrs, numtrans):
    size = 0
    size = size + np.log2(numtrans + 1)
    for att in cte['attributes']:
        if len(cte['attributes']) > 1:
            size = size + np.log2(numattrs)
    return size


def codeTableSize(ct, numattrs, numtrans):
    size1 = 0
    size2 = 0
    for cte in ct:
        size1 = size1 + codeSize1(cte, numattrs, numtrans)
        size2 = size2 + codeSize2(cte, numattrs, numtrans)
    return 1 + min(size1, size2)


def randomCategories(data, k):
    d = {c: [] for c in range(0, k)}
    for uuid in data:
        d[random.randint(0, k - 1)].append(uuid)
    return d


def getCodeTable(ctfile):
    with open(ctfile) as f:
        reader = csv.reader(f)
        header = next(reader)[1:]
        return [{'attributes': {x[1] for x in zip(row[1:], header)
                                if x[0] == '1'},
                 'cost': float(row[0])} for row in reader]


def getModels(filestem, k):
    models = []
    for i in range(k):
        ctfile = '%s.%s.code.csv' % (filestem, str(i))
        models = models + [getCodeTable(ctfile)]
    return models


def scoreTransactionWithCodeTable(atts, ct):
    cost = 0.0
    atts = atts.copy()
    for cte in ct:
        if atts == set():
            break
        if cte['attributes'] <= atts:
            cost += cte['cost']
            atts -= cte['attributes']
    return cost


def reassignCategories(data, models, k):
    d = {c: [] for c in range(0, k)}
    for uuid in data.keys():
        scores = [scoreTransactionWithCodeTable(data[uuid], model)
                  for model in models]
        d[np.argmin(scores)].append(uuid)
    return d


def getClassScores(d):
    freqs = {i: len(d[i]) + 1 for i in d.keys()}
    s = sum(freqs.values())
    class_scores = {i: -np.log2(freqs[i] / s) for i in freqs.keys()}
    return class_scores


def getContext(ctxtfile):
    with open(ctxtfile) as infile:
        reader = csv.reader(infile)
        ctxt = context.Context(reader)
    return ctxt


def writeRow(outfile, ctxt, uuid):
    outfile.write('%s,%s\n' % (uuid, ','.join([str(ctxt.data[uuid][att]) for att in ctxt.header])))


def writeSplits(d, ctxt, filestem):
    for i in d.keys():
        with open('%s.%s.csv' % (filestem, i), 'w') as outfile:
            outfile.write("Object_ID,%s\n" % ','.join(ctxt.header))
            for uuid in d[i]:
                writeRow(outfile, ctxt, uuid)


def scoreSplitsKrimp(d, filestem):
    for i in d.keys():
        krimp.run('%s.%s.csv' % (filestem, str(i)),
                  '%s.%s.scored.csv' % (filestem, str(i)),
                  '%s.%s.code.csv' % (filestem, str(i)))


def mergeSplits(d, filestem):
    sc = dict()
    for i in d.keys():
        with open('%s.%s.scored.csv' % (filestem, i)) as infile:
            reader = csv.reader(infile)
            sc[i] = check.Scores(reader)
    class_scores = getClassScores(d)
    return (sc, class_scores)


def totalCost(sc, class_scores):
    total = 0.0
    for i in sc.keys():
        for (uuid, score) in sc[i].data:
            total = total + score + class_scores[i]
    return total


def writeMerged(sc, class_scores, outfile):
    with open(outfile, 'w') as outfile:
        outfile.write("uuid,score\n")
        for i in sc.keys():
            for (uuid, score) in sc[i].data:
                outfile.write("%s,%f,%s\n" % (uuid, score + class_scores[i], i))


if __name__ == '__main__':
    args = parser.parse_args()
    start = datetime.now()

    prefix = '/tmp/krimpstar%s' % random.randint(1, 10000000)
    filestem = prefix + '/ctxt'
    os.mkdir(prefix)

    ctxt = getContext(args.input)
    n = len(ctxt.data)
    m = len(ctxt.header)
    data = {uuid: {att for att in ctxt.data[uuid].keys()
                   if ctxt.data[uuid][att] == 1}
            for uuid in ctxt.data.keys()}
    dbinitialsize = '-'

    # Full version of the algorithm (selecting the best-fitting number of clusters after scanning up to k)
    if args.mode == 'batch':

        totalmincost = None
        for k in range(1, args.classes + 1):
            d = randomCategories(ctxt.data.keys(), k)  # Alocate randomly the points into k clusters
            print('k = %d' % k)
            lastcost = None
            for i in range(10):
                writeSplits(d, ctxt, filestem)
                scoreSplitsKrimp(d, filestem)
                models = getModels(filestem, k)
                (sc, class_scores) = mergeSplits(d, filestem)
                total = totalCost(sc, class_scores)
                print('%f' % total)
                if lastcost != None and total > lastcost * (1 - args.epsilon):
                    break
                lastcost = total
                d = reassignCategories(data, models, k)

            modelCost = sum([codeTableSize(model, m, len(d[cl]))
                             for (model, cl) in zip(models, d.keys())])
            print('Model cost: %f' % modelCost)
            total_cost = modelCost + total
            print('Total cost: %f' % total_cost)
            if totalmincost == None or total_cost < totalmincost:
                min_k = k
                totalmincost = total_cost
                modelmincost = modelCost
                dbmincost = total
                min_sc = sc
                min_class_scores = class_scores

            # Get initial dataset size
            if k == 1:
                with open('%s/ctxt.0.scored.csv' % prefix) as f:
                    reader = csv.reader(f)
                    dbinitialsize = yaml.safe_load(','.join(reader.__next__()))['dbInitialSize']

        print('Min k = %d with cost %f (modelCost = %f, dbcost = %f, dbinitialsize = %s)' % (
            min_k, totalmincost, modelmincost, dbmincost, dbinitialsize))

        writeMerged(min_sc, min_class_scores, args.output)

    # Simple version (only using k clusters)
    else:
        k = args.classes
        d = randomCategories(ctxt.data.keys(), k)  # Alocate randomly the points into k clusters
        print('Fixed K = %d' % k)
        lastcost = None
        for i in range(10):
            writeSplits(d, ctxt, filestem)
            scoreSplitsKrimp(d, filestem)
            models = getModels(filestem, k)
            (sc, class_scores) = mergeSplits(d, filestem)
            total = totalCost(sc, class_scores)
            print('%f' % total)
            if lastcost != None and total > lastcost * (1 - args.epsilon):
                break
            lastcost = total
            d = reassignCategories(data, models, k)

        modelCost = sum([codeTableSize(model, m, len(d[cl]))
                         for (model, cl) in zip(models, d.keys())])
        print('Model cost: %f' % modelCost)
        total_cost = modelCost + total
        print('Total cost: %f' % total_cost)

        print('Min k = %d with cost %f (modelCost = %f, dbcost = %f, dbinitialsize = %s)' % (
            k, total_cost, modelCost, total, dbinitialsize))

        writeMerged(sc, class_scores, args.output)
        dbmincost = total
        modelmincost = modelCost
        totalmincost = total_cost

    shutil.rmtree(prefix)

    end = datetime.now()

    write_parameters(args.output, end - start, {'Algorithm': 'krimpstar',
                                                'Mode': args.mode,
                                                'dbInitialSize': dbinitialsize,
                                                'kmax': args.classes,
                                                'dbCompressedSize': dbmincost,
                                                'modelSize': modelmincost,
                                                'totalSize': totalmincost})
