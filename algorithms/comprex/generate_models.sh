#!/bin/bash
PATH_COMPREX=algorithms/comprex
python3 $PATH_COMPREX/convertmat.py -i $1 -o $2/input.mat
cd $PATH_COMPREX
matlab -nodesktop -nosplash -r "x = textread('$2/input.mat'); [cost CT] =  buildModelVar(x, 'tmp', 1, $3, '$2'); exit" > $2/model.log

# Arguments:
# 1: input datafile (in .csv format)
# 2: working path
# 3: max iterations