import argparse
import csv
import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

# import algorithms.util.check as c

# simple-minded merging
# input: context CSV and score CSV
# first colimn of score CSV is position of object in context CSV
# output: score CSV with uuids from context replacing first column of input scores

parser = argparse.ArgumentParser(description='Merge score and context CSV')
parser.add_argument('--input', '-i',
					help='input file', required=True)
parser.add_argument('--context', '-c',
					help='context file', required=True)
parser.add_argument('--output', '-o',
					help='output file', required=True)

def merge(input,context,output):
	with open(input) as score_file:
		score_reader = csv.reader(score_file)
		header = next(score_reader)[1:]
		scores = [float(row[1])
					for row in score_reader]

	with open(context) as context_file, open(output,'w') as out_file:
		context_reader = csv.reader(context_file)
		idx = 0
		header = next(context_reader)[1:]
		out_file.write("Object_ID, Score\n")
		for row in context_reader:
			out_file.write("%s,%f\n" % (row[0],scores[idx]))
			idx = idx+1

if __name__ == '__main__':
	args = parser.parse_args()
	merge(args.input,args.context,args.output)


