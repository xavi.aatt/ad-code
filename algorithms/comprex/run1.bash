#!/bin/bash
cd algorithms/comprex
matlab -nodesktop -nosplash -r "x = textread('$1'); [cost CT] =  buildModelVar(x, 'tmp', 1, $3, '$4'); exit" > $4/model.log
echo "Object ID, CompreX Score" > $2
matlab -nodesktop -nosplash -r "computeCompressionScoresVar('$1','$4/CT_tmp.mat','$2'); exit" >> $4/score.log
