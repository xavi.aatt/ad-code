#!/bin/sh

PATH_TMP=$4
PATH_COMPREX=algorithms/comprex

mkdir $PATH_TMP

python3 $PATH_COMPREX/convertmat.py -i $1 -o $PATH_TMP/input.mat

echo "$1" > $1.out
/usr/bin/time bash $PATH_COMPREX/run1.bash $PATH_TMP/input.mat $PATH_TMP/scores.csv $3 $PATH_TMP 2>> $PATH_TMP/times.out

python3 $PATH_COMPREX/merge.py -i $PATH_TMP/scores.csv -c $1 -o $2
