#!/bin/bash
PATH_COMPREX=algorithms/comprex
cd $PATH_COMPREX
echo "Object ID, CompreX Score" > $2/scores.csv
matlab -nodesktop -nosplash -r "computeCompressionScoresVar('$1','$2/CT_tmp.mat','$2/scores.csv'); exit" >> $2/score.log

# Arguments:
# 1: entire dataset to score using that particular model
# 2: working path