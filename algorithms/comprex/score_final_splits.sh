#!/bin/bash
PATH_COMPREX=algorithms/comprex
cd $PATH_COMPREX
rm -f $1/finalscores.csv
echo "Object ID, CompreX Score" > $1/finalscores.csv
matlab -nodesktop -nosplash -r "computeCompressionScoresVar('$1/input.mat','$1/CT_tmp.mat','$1/finalscores.csv'); exit" >> $1/finalscore.log

python3 merge.py -i $1/finalscores.csv -c $1/cluster.csv -o $1/mergedscores.csv


# Arguments:
# 1: working path