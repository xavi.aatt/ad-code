data_dir = config['data']
result_dir = config['results']
metrics_file = config['metrics']
timeout_slim = config['timeout_slim']
scored_or_ranked = config['scored_or_ranked']
processes_to_combine = config['processes_to_combine']
slims_to_combine = ['slim0D', 'slim1D', 'slim2D', 'slimOC3']


rule all:
    input:  # Here we put all files we want to obtain
        list = [f'{result_dir}/{context}.{algorithm}.{mode}.{scored_or_ranked}.csv'
        for context in config['context']
        for algorithm in config['algorithm']
        for mode in config['mode']
            if (algorithm == 'klmeans' or mode != 'fastbatch')  # do not consider fastbatch in other algorithms than klmeans
            and (algorithm not in ['krimp', 'comprex', 'slim0D', 'slim1D', 'slim2D', 'slimOC3', 'slimbaseline2D',
            'slimcombineduniform', 'slimcombinedweighted', 'slimcombinedmaxscore', 'slimcombinedaverage', 'slimcombinedgeometric',
            'slimcombinedmedian', 'slimcombinedminrank'] or mode == 'batch')  # only consider them with mode batch
            and (algorithm not in ['krimpstar', 'comprexstar'] or mode in ['batch', 'simple'])
        ]


rule klmeansscore:
    wildcard_constraints:
        mode='batch|fastbatch|stream|simple'
    input:
        "%s/{context}.csv" % data_dir
    output:
        "%s/{context}.klmeans.{mode}.scored.csv" % result_dir
    shell:
	    "python3 algorithms/klmeans.py -k %d -m {wildcards.mode} -i {input} -o {output}" % config['k_means']


rule krimpscore:
    wildcard_constraints:
        mode='batch'
    input:
        "%s/{context}.csv" % data_dir
    output:
        "%s/{context}.krimp.{mode}.scored.csv" % result_dir
    shell:
	    "python3 algorithms/krimp.py -i {input} -o {output} -t %s -d /disk/scratch/Code/iAnomaly1.0/tmp -s %s" % (config['itemsettype'], config['minsup'])


rule krimpstarscore:
    wildcard_constraints:
        mode='batch|simple'
    input:
        "%s/{context}.csv" % data_dir
    output:
        "%s/{context}.krimpstar.{mode}.scored.csv" % result_dir
    shell:
	    "python3 algorithms/krimpstar.py -i {input} -o {output} -m {wildcards.mode} -k %d" % config['k_star']


# rule slimscore:
#     wildcard_constraints:
#         mode='batch'
#     input:
#         "%s/{context}.csv" % data_dir
#     output:
#         "%s/{context}.slim.{mode}.scored.csv" % result_dir
#     shell:
# 	    "python3 algorithms/slim.py -i {input} -o {output}"
#
#
# rule slimplusscore:
#     wildcard_constraints:
#         mode='batch'
#     input:
#         "%s/{context}.csv" % data_dir
#     output:
#         "%s/{context}.slimplus.{mode}.scored.csv" % result_dir
#     shell:
# 	    "python3 algorithms/slim.py -i {input} -o {output} -m plus"


rule slimscore:
    wildcard_constraints:
        mode='batch',
        method='0D|1D|2D|OC3|baseline2D'
    input:
        "%s/{context}.csv" % data_dir
    output:
        "%s/{context}.slim{method}.{mode}.scored.csv" % result_dir
    shell:
	    "python3 algorithms/slim_upc.py -i {input} -o {output} -m {wildcards.method} -t %s" % timeout_slim


rule comprexscore:
    wildcard_constraints:
        mode='batch'
    input:
        "%s/{context}.csv" % data_dir
    output:
        "%s/{context}.comprex.{mode}.scored.csv" % result_dir
    shell:
	    "python3 algorithms/comprex.py -i {input} -o {output} -m %s" % config['maxit']


rule comprexstarscore:
    wildcard_constraints:
        mode='batch|simple'
    input:
        "%s/{context}.csv" % data_dir
    output:
        "%s/{context}.comprexstar.{mode}.scored.csv" % result_dir
    shell:
	    "python3 algorithms/comprexstar.py -i {input} -o {output} -mo {wildcards.mode} -k %s -m %s" % (config['k_comp'], config['maxit'])



rule combineslim:
    input:
        ['%s/{context}.%s.batch.scored.csv' % (result_dir, algorithm)
	 for algorithm in slims_to_combine]
    output:
        '%s/{context}.slimcombined{aggr}.{mode}.scored.csv' % result_dir
    shell:
        'python3 algorithms/combine.py {input} -a {wildcards.aggr} -o {output}'


rule combineprocess:
    wildcard_constraints:
        aggr='uniform|weighted|maxscore|average|geometric|minrank|median',
        algorithm='avf|avc|krimp|krimpstar|klmeans|comprex|slim0D|slim1D|slim2D|slimOC3'
    input:
        ['%s/{dir}/%s.{algorithm}.{mode}.scored.csv' % (result_dir, process)
	 for process in processes_to_combine]
    output:
        '%s/{dir}/ProcessCombined_{aggr}.{algorithm}.{mode}.scored.csv' % result_dir
    params:
        reverse = lambda wildcards, output: '' if (wildcards.aggr in ['average', 'geometric', 'minrank', 'median'] or wildcards.algorithm == 'avf') else '-r'
    shell:
        'python3 algorithms/combine.py {input} -a {wildcards.aggr} -o {output} {params.reverse}'


rule frequencyscore:
    wildcard_constraints:
        algorithm='avf|avc',
        mode='batch|stream'
    input:
        '%s/{context}.csv' % data_dir
    output:
        '%s/{context}.{algorithm}.{mode}.scored.csv' % result_dir
    shell:
        'python3 algorithms/ad.py -i {input} -o {output} -s {wildcards.algorithm} -m {wildcards.mode}'


rule check:
    input:
        scores='%s/{dir}/{dataset}.{algorithm}.{mode}.scored.csv' % result_dir,
        groundtruth='%s/{dir}/groundtruth.csv' % data_dir,  # A groundtruth file must be available
    output:
        ranks="%s/{dir}/{dataset}.{algorithm}.{mode}.ranked.csv" % result_dir
    params:
        reverse = lambda wildcards, output: '' if (wildcards.algorithm == 'avf' and wildcards.dataset not in ['ProcessCombined_uniform', 'ProcessCombined_weighted', 'ProcessCombined_maxscore']) else '-r'
    shell:
        'python3 algorithms/check.py -i {input.scores} -g {input.groundtruth} -o {output.ranks} -m %s/metrics/%s.csv {params.reverse}' % (result_dir, metrics_file) # -t AdmSubject::Node if you did not modify groundtruth files