## Datasets directory

You should put the databases you want to test here. It is recommended to
have them organized in folders. Remember that the structure of folders you 
have for a certain dataset corresponds to `{context}` for Snakefile, and
it is the same one that the results will follow.

The script `merge_gt.py` is for merging and filtering the grountruth files of 
`e2data` from DARPA.